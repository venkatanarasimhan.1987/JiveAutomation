/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jive.jiveticketingsystem;

import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author venkata_narasimhan
 */
public class JiveExpectedStrings {

    static HashSet ExpectedString = new HashSet();
    HashSet tempExpectedString = new HashSet();

    public HashSet addAllString(String title) {

        //refactor with just 1 list string since I am using only case statements
        ArrayList<String> listStrings = new ArrayList<>();

        switch (title) {
            case "header":
                //Header Text 1
                listStrings.add("Products");
                listStrings.add("Pricing");
                listStrings.add("Partners");
                listStrings.add("Company");
                listStrings.add("Support");
                listStrings.add("877-548-3007");
                listStrings.add("Log in");
                listStrings.add("Get a Quote");
                tempExpectedString.addAll(listStrings);
                break;

            case "mainbody":
                //Main body center 2
                listStrings.add("Submit a Ticket");
                listStrings.add("Example: Network Readiness");
                tempExpectedString.addAll(listStrings);
                break;

            case "sectionleft":
                //Secondary section left side 3 
                listStrings.add("Resources");
                listStrings.add("Support");
                listStrings.add("Submit a Ticket");
                listStrings.add("Status");
                listStrings.add("Manuals");
                listStrings.add("Release Notes");
                tempExpectedString.addAll(listStrings);
                break;

            case "center":
                //Message Placeholder center 4 wufooFormm7x4z5
                listStrings.add("Tell us a little about your issue:");
                listStrings.add("Name");
                listStrings.add("Email");
                listStrings.add("Company Name");
                listStrings.add("Best Contact Number");
                listStrings.add("Next Page");
                listStrings.add("###");
                listStrings.add("###");
                listStrings.add("####");
                listStrings.add("1");
                listStrings.add("2");
                listStrings.add("/");
                tempExpectedString.addAll(listStrings);
                break;

            case "footer1":
                //Footer of the page Part1 5
                listStrings.add("Get to Know Us");
                listStrings.add("Blog");
                listStrings.add("Contact Us");
                listStrings.add("Legal");
                listStrings.add("Videos");
                tempExpectedString.addAll(listStrings);
                break;

            case "footer2":
                //Footer of the page Part2 6
                listStrings.add("Jive Life");
                listStrings.add("Careers");
                listStrings.add("Customers");
                listStrings.add("Racing");
                listStrings.add("Press");
                tempExpectedString.addAll(listStrings);
                break;

            case "footer3":
                //Footer of the page Part3 7
                listStrings.add("Products");
                listStrings.add("Jive Voice");
                listStrings.add("Jive Contact Center");
                listStrings.add("Jive Video");
                listStrings.add("Jive Apps");
                tempExpectedString.addAll(listStrings);
                break;

            case "footer4":
                //Footer of the page Part4 8
                listStrings.add("Awesome Links");
                listStrings.add("VoIP Phones");
                listStrings.add("Support");
                listStrings.add("Integrations");
                listStrings.add("Status");
                tempExpectedString.addAll(listStrings);
                break;

            case "footer5":
                //Footer of the page Part5 9
                listStrings.add("Change Region");
                listStrings.add("© 2017 Jive Resource Center");
                listStrings.add("Privacy Policy");
                tempExpectedString.addAll(listStrings);
                break;

            case "informationaltext":
                //Informational text 10
                listStrings.add("Jive’s office phone systems integrate with many third-party cloud applications such as Salesforce, "
                        + "Redtail and more. Jive also includes custom features for specific industries including");
                listStrings.add("higher education");
                listStrings.add("insurance");
                listStrings.add("non-profit");
                listStrings.add("hospitality");
                listStrings.add("Jive’s hosted");
                listStrings.add("business VoIP services");
                listStrings.add("work on");
                listStrings.add("VoIP phone");
                listStrings.add("and");
                listStrings.add("IP phone</a> hardware from Polycom, Cisco, Panasonic, VTech, and Yealink. Jive Cloud, "
                        + "a cloud platform based on open standards, delivers the most powerful and economical business "
                        + "communication services in the industry.");
                tempExpectedString.addAll(listStrings);
                break;

            case "informationalTextURL":
                //URLS
                listStrings.add("https://jive.com/resources/education/k12");
                listStrings.add("https://jive.com/resources/education/higher-ed/");
                listStrings.add("https://jive.com/resources/small-business/");
                listStrings.add("https://jive.com/resources/government");
                listStrings.add("https://jive.com/resources/hospitality");
                listStrings.add("https://jive.com/resources/phones");

        }
        ExpectedString.addAll(tempExpectedString);
        return ExpectedString;
    }

}
