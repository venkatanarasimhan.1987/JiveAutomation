package com.jive.jiveticketingsystem;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import io.restassured.RestAssured;

/**
 *
 * @author venkata_narasimhan
 */
public class JiveHttpResponseCode {
    
    public int httpResponseCodeViaGet(String url) {
            return RestAssured.get(url).statusCode();
    }

    public int httpResponseCodeViaPost(String url) {
        return RestAssured.post(url).statusCode();
    }
}
