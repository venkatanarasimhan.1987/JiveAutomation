/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.jive.jiveticketingsystem.JiveExpectedStrings;
import com.jive.jiveticketingsystem.JiveHttpResponseCode;
import java.util.HashSet;
import java.util.Set;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


/**
 *
 * @author venkata_narasimhan
 */
public class JiveHomePageTest {

    private static WebDriver driver = null;
    public static String URL = "https://jive.com/resources/support/submit-a-ticket/";

    @Test
    public void test_CheckIfPageIsAvailable() throws Exception {

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver();
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        JiveHttpResponseCode obj = new JiveHttpResponseCode();
        int ResponseValue = obj.httpResponseCodeViaGet(URL);
        assertThat("Expected Response of" + ResponseValue + "was found on page", ResponseValue == 200);
        driver.close();
    }

    @Test
    public void test_HomePageForAllText_Header() throws Exception {
        JiveExpectedStrings obj = new JiveExpectedStrings();

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver();
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);

        Set<String> ExpectedString = new HashSet();
        ExpectedString.addAll(obj.addAllString("header"));

        for (String s : ExpectedString) {
            System.out.println(s);
            boolean doesItExist = driver.getPageSource().contains(s);
            if (doesItExist == false) {
                assertThat("The String does not exist", false);
            }
        }
        driver.close();
    }   
       
    @Test
    public void test_HomePageForAllText_MainBodyCenter() throws Exception {
        JiveExpectedStrings obj = new JiveExpectedStrings();

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver();
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);

        Set<String> ExpectedString = new HashSet();
        ExpectedString.addAll(obj.addAllString("mainbody"));

        for (String s : ExpectedString) {
            System.out.println(s);
            boolean doesItExist = driver.getPageSource().contains(s);
            if (doesItExist == false) {
                assertThat("The String does not exist", false);
            }
        }
        driver.close();
    }
    
    @Test
    public void test_HomePageForAllText_SecondarySectionLeftside() throws Exception {
        JiveExpectedStrings obj = new JiveExpectedStrings();

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver();
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);

        Set<String> ExpectedString = new HashSet();
        ExpectedString.addAll(obj.addAllString("sectionleft"));

        for (String s : ExpectedString) {
            System.out.println(s);
            boolean doesItExist = driver.getPageSource().contains(s);
            if (doesItExist == false) {
                assertThat("The String does not exist", false);
            }
        }
        driver.close();
    }
    
    @Test
    public void test_HomePageForAllText_MessagePlaceholderCenter() throws Exception {
        JiveExpectedStrings obj = new JiveExpectedStrings();

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");
        
        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        Thread.sleep(5000);
        Set<String> ExpectedString = new HashSet();
        ExpectedString.addAll(obj.addAllString("center"));

        String iFrameContainer = driver.switchTo().frame("wufooFormm7x4z5").getPageSource();
        
        for (String s : ExpectedString) {
            System.out.println(s);                   
            boolean doesItExist = iFrameContainer.contains(s);          
            
            if (doesItExist == false) {
                assertThat("The String does not exist", false);
            }
        }
        driver.close();
    }
    
    @Test
    public void test_HomePageForAllText_FooterOfThePagePart1() throws Exception {
        JiveExpectedStrings obj = new JiveExpectedStrings();

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver();
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);

        Set<String> ExpectedString = new HashSet();
        ExpectedString.addAll(obj.addAllString("footer1"));

        for (String s : ExpectedString) {
            System.out.println(s);
            boolean doesItExist = driver.getPageSource().contains(s);
            if (doesItExist == false) {
                assertThat("The String does not exist", false);
            }
        }
        driver.close();
    }
    
    @Test
    public void test_HomePageForAllText_FooterOfThePagePart2() throws Exception {
        JiveExpectedStrings obj = new JiveExpectedStrings();

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver();
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);

        Set<String> ExpectedString = new HashSet();
        ExpectedString.addAll(obj.addAllString("footer2"));

        for (String s : ExpectedString) {
            System.out.println(s);
            boolean doesItExist = driver.getPageSource().contains(s);
            if (doesItExist == false) {
                assertThat("The String does not exist", false);
            }
        }
        driver.close();
    }
    
    @Test
    public void test_HomePageForAllText_FooterOfThePagePart3() throws Exception {
        JiveExpectedStrings obj = new JiveExpectedStrings();

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver();
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);

        Set<String> ExpectedString = new HashSet();
        ExpectedString.addAll(obj.addAllString("footer3"));

        for (String s : ExpectedString) {
            System.out.println(s);
            boolean doesItExist = driver.getPageSource().contains(s);
            if (doesItExist == false) {
                assertThat("The String does not exist", false);
            }
        }
        driver.close();
    }
    
    @Test
    public void test_HomePageForAllText_FooterOfThePagePart4() throws Exception {
        JiveExpectedStrings obj = new JiveExpectedStrings();

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver();
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);

        Set<String> ExpectedString = new HashSet();
        ExpectedString.addAll(obj.addAllString("footer4"));

        for (String s : ExpectedString) {
            System.out.println(s);
            boolean doesItExist = driver.getPageSource().contains(s);
            if (doesItExist == false) {
                assertThat("The String does not exist", false);
            }
        }
        driver.close();
    }
    
    @Test
    public void test_HomePageForAllText_FooterOfThePagePart5() throws Exception {
        JiveExpectedStrings obj = new JiveExpectedStrings();

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver();
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);

        Set<String> ExpectedString = new HashSet();
        ExpectedString.addAll(obj.addAllString("footer5"));

        for (String s : ExpectedString) {
            System.out.println(s);
            boolean doesItExist = driver.getPageSource().contains(s);
            if (doesItExist == false) {
                assertThat("The String does not exist", false);
            }
        }
        driver.close();
    }
    
    @Test
    public void test_HomePageForAllText_Informationtext() throws Exception {
        JiveExpectedStrings obj = new JiveExpectedStrings();
        
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver();
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);

        Set<String> ExpectedString = new HashSet();
        ExpectedString.addAll(obj.addAllString("informationaltext"));

        for (String s : ExpectedString) {
            System.out.println(s);
            boolean doesItExist = driver.getPageSource().contains(s);
            if (doesItExist == false) {
                assertThat("The String does not exist", false);
            }
        }
        driver.close();
    }
    
    @Test
    public void test_HomePageForAllText_InformationtextURLs() throws Exception {
        JiveExpectedStrings obj = new JiveExpectedStrings();
        
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver();
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);

        Set<String> ExpectedString = new HashSet();
        ExpectedString.addAll(obj.addAllString("informationalTextURL"));

        for (String s : ExpectedString) {
            System.out.println(s);
            boolean doesItExist = driver.getPageSource().contains(s);
            if (doesItExist == false) {
                assertThat("The String does not exist", false);
            }
        }
        driver.close();
    }

}
