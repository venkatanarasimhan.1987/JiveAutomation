/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author venkata_narasimhan
 */
public class JiveAllStrings {
    
    //All proper Names
    public static String NAMEVALUE = "Jazz Johnson";
    public static String EMAILVALUE = "Jazz@Johnson.com";
    public static String COMPANYNAMEVALUE = "Jazz Johnson Company";
    public static String PHONENUMBERVALUEA = "102";
    public static String PHONENUMBERVALUEB = "345";
    public static String PHONENUMBERVALUEC = "6789";
    public static String DESCRIPTIONMINI = "Jazz Johnson is adding a mini description";
    public static String DESCRIPTIONMESSAGE = "Jazz Johnson is adding a big description,Jazz Johnson is adding a big "
            + "description,Jazz Johnson is adding a big description,Jazz Johnson is adding a big description,Jazz "
            + "Johnson is adding a big description,Jazz Johnson is adding a big description";
    
    //All blanks
    public static String NAMEVALUE_BLANK = "";
    public static String EMAILVALUE_BLANK = "";
    public static String COMPANYNAMEVALUE_BLANK = "";
    public static String PHONENUMBERVALUEA_BLANK = "";
    public static String PHONENUMBERVALUEB_BLANK = "";
    public static String PHONENUMBERVALUEC_BLANK = "";
    public static String DESCRIPTIONMINI_BLANK = "";
    public static String DESCRIPTIONMESSAGE_BLANK = "";
    
    //Error Cases
    public static String NAMEVALUE_ERROR = "AS5454A$%";
    public static String EMAILVALUE_ERROR = "AS5454A$%@AS5454A$%.AS5454A$%";
    public static String COMPANYNAMEVALUE_ERROR = "AS5454A$%@_*";
    public static String PHONENUMBERVALUEA_ERROR = "AS5";
    public static String PHONENUMBERVALUEB_ERROR = "%^&";
    public static String PHONENUMBERVALUEC_ERROR = "A1*%";
    public static String DESCRIPTIONMINI_ERROR = "Jazz Johnson is A1*% adding a mini description";
    public static String DESCRIPTIONMESSAGE_ERROR = "Jazz Johnson is A1*% adding a big description,Jazz Johnson is adding a big "
            + "description,Jazz Johnson is adding a big description,Jazz A1*% Johnson is adding a big description,Jazz "
            + "Johnson is adding a big description,Jazz A1*% Johnson is adding a big description";
    
    //French Support
    public static String NAMEVALUE_FR = "JacÇéâêîôob Çéâêîôûàèùëïüéè";
    public static String EMAILVALUE_FR = "Çéâêîô@gmail.com";
    public static String COMPANYNAMEVALUE_FR = "Çéâêîôûàè";
    public static String DESCRIPTIONMINI_FR = "ÇéâêîôûàèÇéâêîôûàèÇéâêîôûàèÇéâêîôûàè";
    public static String DESCRIPTIONMESSAGE_FR = "ÇéâêîôûàèÇéâêîôûàèÇéâêîôûàèÇéâêîôûàèÇéâêîôûàèÇéâêîôûàèÇéâêîôûàèÇéâêîôûàè"
            + "ÇéâêîôûàèÇéâêîôûàèÇéâêîôûàèÇéâêîôûàèÇéâêîôûàèÇéâêîôûàèÇéâêîôûàèÇéâêîôûàè"
            + "ÇéâêîôûàèÇéâêîôûàèÇéâêîôûàèÇéâêîôûàèÇéâêîôûàèÇéâêîôûàèÇéâêîôûàèÇéâêîôûàè";    
    
    //Miscellaneous
    public static String ERRORMESSAGEINMODAL1 = "This field is required. Please enter a value.";
    public static String ERRORMESSAGEINMODAL2 = "There was a problem with your submission.";
    public static String ERRORMESSAGEINMODAL3 = "Errors have been <b>highlighted</b> below.";
    public static String ERRORMESSAGEINMODALFORPHONE = "Please enter a valid phone number.";
    
    //255chars or more
    public static String NAMEVALUE_255 = "Jazz JohnsonJazz JohnsonJazz JohnsonJazz JohnsonJazz JohnsonJazz "
            + "JohnsonJazz JohnsonJazz JohnsonJazz Johnson public static String NAMEVALUE_255 JohnsonJazz "
            + "JohnsonJazz JohnsonJazz JohnsonJazz JohnsonJazz JohnsonJazz JohnsonJazz JohnsonJazz Johnson";
    
    public static String EMAILVALUE_255 = "JazzJohnsonJazzJohnJohnsonJazzJohnsonJazzJohnsonJazzJohnsonJazzJohnsonJazzsonJazzJohnsonJazz@JohnsonJohnsonJazzJohJohnsonJazzJohnsonJazzJohnsonJazzJohnsonJazznsonJazzJohnsonJazz.comJohnsonJazzJohnsonJazzJohnsonJazzJohnsonJazzJohnsonJazzhnsonJazzJohnsonJazzJohnsonJazz";
    public static String COMPANYNAMEVALUE_255 = "Jazz Johnson Compan JazzJohnsonJazzJohnJohnsonJazzJohnsonJazzJohnsonJazzJohnsonJazzJazzJohnsonJazzJohnJohnsonJazzJohnsonJazzJohnsonJazzJohnsonJazzJazzJohnsonJazzJohnJohnsonJazzJohnsonJazzJohnsonJazzJohnsonJazzJazzJohnsonJazzJohnJohnsonJazzJohnsonJazzJohnsonJazzJohnsonJazzJazzJohnsonJazzJohnJohnsonJazzJohnsonJazzJohnsonJazzJohnsonJazz";
    public static String PHONENUMBERVALUEA_255 = "100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
}
