/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.Color;
import org.junit.Test;
import junit.framework.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 *
 * @author venkata_narasimhan
 */
public class JivePhoneNumbersOnly {

    private static WebDriver driver = null;
    public static String URL = "https://jive.com/resources/support/submit-a-ticket/";

    @Test
    public void test_CheckforNumberValuesInPhoneNumber() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the 2nd Page is open - FINAL", driver.findElement(By.id("saveForm")).isDisplayed());

        driver.close();
    }

    @Test
    public void test_CheckforNumberValuesInPhoneNumberTextBox1WithLetters() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA_ERROR);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));

        driver.close();
    }

    @Test
    public void test_CheckforNumberValuesInPhoneNumberTextBox2WithLetters() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB_ERROR);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));

        driver.close();
    }

    @Test
    public void test_CheckforNumberValuesInPhoneNumberTextBox3WithLetters() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC_ERROR);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));

        driver.close();
    }

    @Test
    public void test_CheckforNumberValuesInPhoneNumberTextBox12WithLetters() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA_ERROR);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB_ERROR);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));

        driver.close();
    }

    @Test
    public void test_CheckforNumberValuesInPhoneNumberTextBox13WithLetters() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA_ERROR);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC_ERROR);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));

        driver.close();
    }

    @Test
    public void test_CheckforNumberValuesInPhoneNumberTextBox23WithLetters() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB_ERROR);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC_ERROR);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));

        driver.close();
    }
    
    @Test
    public void test_CheckforNumberValuesInPhoneNumberTextBox123WithLetters() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA_ERROR);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB_ERROR);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC_ERROR);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));

        driver.close();
    }
    
    @Test
    public void test_CheckforNumberValuesInPhoneNumberTextBoxNull1WithLetters() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA_BLANK);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));

        driver.close();
    }
    
    @Test
    public void test_CheckforNumberValuesInPhoneNumberTextBoxNull2WithLetters() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB_BLANK);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));

        driver.close();
    }
    
    @Test
    public void test_CheckforNumberValuesInPhoneNumberTextBoxNull12WithLetters() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA_BLANK);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB_BLANK);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));

        driver.close();
    }
    
    @Test
    public void test_CheckforNumberValuesInPhoneNumberTextBoxNull13WithLetters() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA_BLANK);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC_BLANK);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));

        driver.close();
    }
    
    @Test
    public void test_CheckforNumberValuesInPhoneNumberTextBoxNull23WithLetters() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB_BLANK);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC_BLANK);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));

        driver.close();
    }
    
    @Test
    public void test_CheckforNumberValuesInPhoneNumberTextBoxNull123WithLetters() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA_BLANK);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB_BLANK);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC_BLANK);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the 2nd Page is open - FINAL", driver.findElement(By.id("saveForm")).isDisplayed());

        driver.close();
    }

}
