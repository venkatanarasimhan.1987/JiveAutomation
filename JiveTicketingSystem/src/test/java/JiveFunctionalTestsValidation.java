/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Random;
import junit.framework.Assert;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.openqa.selenium.WebElement;

/**
 *
 * @author venkata_narasimhan
 */
public class JiveFunctionalTestsValidation {

    private static WebDriver driver = null;
    public static String URL = "https://jive.com/resources/support/submit-a-ticket/";

    @Test
    public void test_formFillingFillAll() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();

        //Second page populate only 1 textbox
        driver.findElement(By.id("Field28")).sendKeys(obj.DESCRIPTIONMINI);
        driver.findElement(By.id("Field1")).sendKeys(obj.DESCRIPTIONMESSAGE);

        Assert.assertTrue("Verify that the 2nd Page is open", driver.findElement(By.id("saveForm")).isDisplayed());

        driver.close();
    }

    @Test
    public void test_formFillingFillOnlyName() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();

        //check for error modal for email
        Assert.assertTrue("Verify that the error modal is shown for the email", driver.findElement(By.id("fo2li12")).isDisplayed());
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());

        //check for error modal for company name
        Assert.assertTrue("Verify that the error modal is shown for the company name", driver.findElement(By.id("fo2li14")).isDisplayed());
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());

        Assert.assertTrue("Verify that the error message is shown on the modal head", driver.findElement(By.id("fo2li26")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODAL1));
        Assert.assertTrue("Verify that the error message is shown in modal 2", driver.getPageSource().contains(obj.ERRORMESSAGEINMODAL2));
        Assert.assertTrue("Verify that the error message is shown in modal 3", driver.getPageSource().contains(obj.ERRORMESSAGEINMODAL3));

        driver.close();
    }

    @Test
    public void test_formFillingFillOnlyEmail() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();

        //check for error modal for name
        Assert.assertTrue("Verify that the error modal is shown for the name", driver.findElement(By.id("fo2li26")).isDisplayed());
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());

        //check for error modal for company name
        Assert.assertTrue("Verify that the error modal is shown for the company name", driver.findElement(By.id("fo2li14")).isDisplayed());
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());

        Assert.assertTrue("Verify that the error message is shown on the modal head", driver.findElement(By.id("fo2li26")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODAL1));
        Assert.assertTrue("Verify that the error message is shown in modal 2", driver.getPageSource().contains(obj.ERRORMESSAGEINMODAL2));
        Assert.assertTrue("Verify that the error message is shown in modal 3", driver.getPageSource().contains(obj.ERRORMESSAGEINMODAL3));

        driver.close();
    }

    @Test
    public void test_formFillingFillOnlyCompanyName() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();

        //check for error modal for name
        Assert.assertTrue("Verify that the error modal is shown for the name", driver.findElement(By.id("fo2li26")).isDisplayed());
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());

        //check for error modal for email
        Assert.assertTrue("Verify that the error modal is shown for the company name", driver.findElement(By.id("fo2li12")).isDisplayed());
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());

        Assert.assertTrue("Verify that the error message is shown on the modal head", driver.findElement(By.id("fo2li26")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODAL1));
        Assert.assertTrue("Verify that the error message is shown in modal 2", driver.getPageSource().contains(obj.ERRORMESSAGEINMODAL2));
        Assert.assertTrue("Verify that the error message is shown in modal 3", driver.getPageSource().contains(obj.ERRORMESSAGEINMODAL3));

        driver.close();
    }

    @Test
    public void test_formFillingFillAllWithoutContactNumber() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();

        //Second page populate only 1 textbox
        driver.findElement(By.id("Field28")).sendKeys(obj.DESCRIPTIONMINI);
        driver.findElement(By.id("Field1")).sendKeys(obj.DESCRIPTIONMESSAGE);

        Assert.assertTrue("Verify that the 2nd Page is open", driver.findElement(By.id("saveForm")).isDisplayed());

        driver.close();
    }

    @Test
    public void test_formFillingFillAllWithErrorInContactNumber1() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();

        //check for error modal for contact Number
        Assert.assertTrue("Verify that the error modal is shown for the contact number", driver.findElement(By.id("fo2li27")).isDisplayed());
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown on the modal head", driver.findElement(By.id("fo2li26")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));

        driver.close();
    }

    @Test
    public void test_formFillingFillAllWithErrorInContactNumber2() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();

        //check for error modal for contact Number
        Assert.assertTrue("Verify that the error modal is shown for the contact number", driver.findElement(By.id("fo2li27")).isDisplayed());
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown on the modal head", driver.findElement(By.id("fo2li26")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));

        driver.close();
    }

    @Test
    public void test_formFillingFillAllWithErrorInContactNumber3() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();

        //check for error modal for contact Number
        Assert.assertTrue("Verify that the error modal is shown for the contact number", driver.findElement(By.id("fo2li27")).isDisplayed());
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown on the modal head", driver.findElement(By.id("fo2li26")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));

        driver.close();
    }

    @Test
    public void test_formFillingFillAllWithErrorInContactNumber4() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();

        //check for error modal for contact Number
        Assert.assertTrue("Verify that the error modal is shown for the contact number", driver.findElement(By.id("fo2li27")).isDisplayed());
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown on the modal head", driver.findElement(By.id("fo2li26")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));

        driver.close();
    }

    @Test
    public void test_formFillingFillAllWithErrorInContactNumber5() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();

        //check for error modal for contact Number
        Assert.assertTrue("Verify that the error modal is shown for the contact number", driver.findElement(By.id("fo2li27")).isDisplayed());
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown on the modal head", driver.findElement(By.id("fo2li26")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));

        driver.close();
    }

    @Test
    public void test_formFillingFillAllWithErrorInContactNumber6() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();

        //check for error modal for contact Number
        Assert.assertTrue("Verify that the error modal is shown for the contact number", driver.findElement(By.id("fo2li27")).isDisplayed());
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown on the modal head", driver.findElement(By.id("fo2li26")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));

        driver.close();
    }

    @Test
    public void test_formFillingFillAllPage2Part1() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        
        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("saveForm")).click();

        //Error message validation page 2
        Assert.assertTrue("Verify that the error modal is shown for the brief description", driver.findElement(By.id("fo2li28")).isDisplayed());
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error modal is shown for the full description", driver.findElement(By.id("fo2li1")).isDisplayed());
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODAL1));
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODAL3));
        
        Assert.assertTrue("Verify that the 2nd Page is open", driver.findElement(By.id("saveForm")).isDisplayed());

        driver.close();
    }
    
    @Test
    public void test_formFillingFillAllPage2Part2() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();

        //Second page populate only 1 textbox
        driver.findElement(By.id("Field28")).sendKeys(obj.DESCRIPTIONMINI);        
                
        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("saveForm")).click();
        
        //Error message validation page 2
        Assert.assertTrue("Verify that the error modal is shown for the full description", driver.findElement(By.id("fo2li1")).isDisplayed());
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODAL1));
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODAL3));

        Assert.assertTrue("Verify that the 2nd Page is open", driver.findElement(By.id("saveForm")).isDisplayed());

        driver.close();
    }
    
    @Test
    public void test_formFillingFillAllPage2Part3() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();

        //Second page populate only 1 textbox
        driver.findElement(By.id("Field1")).sendKeys(obj.DESCRIPTIONMESSAGE);
        
        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("saveForm")).click();
        
        //Error message validation page 2
        Assert.assertTrue("Verify that the error modal is shown for the full description", driver.findElement(By.id("fo2li28")).isDisplayed());
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODAL1));
        Assert.assertTrue("Verify that the error message is shown in modal 1", driver.getPageSource().contains(obj.ERRORMESSAGEINMODAL3));       
        
        Assert.assertTrue("Verify that the 2nd Page is open", driver.findElement(By.id("saveForm")).isDisplayed());

        driver.close();
    }
    
    @Test
    public void test_formFillingFillAllAndRedirect() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        driver.findElement(By.id("Field27")).sendKeys(obj.PHONENUMBERVALUEA);
        driver.findElement(By.id("Field27-1")).sendKeys(obj.PHONENUMBERVALUEB);
        driver.findElement(By.id("Field27-2")).sendKeys(obj.PHONENUMBERVALUEC);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();

        //Second page populate only 1 textbox
        driver.findElement(By.id("Field28")).sendKeys(obj.DESCRIPTIONMINI);
        driver.findElement(By.id("Field1")).sendKeys(obj.DESCRIPTIONMESSAGE);

        Assert.assertTrue("Verify that the 2nd Page is open", driver.findElement(By.id("saveForm")).isDisplayed());

        //Navigate to the previous page
        driver.findElement(By.id("previousPageButton")).click();
        Assert.assertTrue("Verify that the 1st Page is open", driver.findElement(By.id("nextPageButton")).isDisplayed());
        
        driver.close();
    }

}
