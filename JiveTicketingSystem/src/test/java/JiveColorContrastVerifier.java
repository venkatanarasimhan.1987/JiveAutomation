/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.Color;
import org.junit.Test;
import junit.framework.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 *
 * @author venkata_narasimhan
 */
public class JiveColorContrastVerifier {

    private static WebDriver driver = null;
    public static String URL = "https://jive.com/resources/support/submit-a-ticket/";
    public static String backGroundColor;
    public static String fontColor;
    Double LuminanceValue1;
    Double LuminanceValue2;

    public String RGBtoHexConverter(String colorValue) {

        String s = colorValue.substring(colorValue.indexOf("(") + 1);
        s = colorValue.substring(0, colorValue.indexOf(")") + 1);

        String r = s.substring(4, 7);
        String g = s.substring(9, 12);
        String b = s.substring(14, 17);

        int rvalue = Integer.parseInt(r);
        int bvalue = Integer.parseInt(b);
        int gvalue = Integer.parseInt(g);
        
        LuminanceValue1 = (0.2126 * rvalue) + (0.7152 * gvalue) + (0.0722 * bvalue);
        System.out.println(LuminanceValue1);
        
        Color color = new Color(rvalue, gvalue, bvalue);
        String hex = Integer.toHexString(color.getRGB() & 0xFFFFFF);
        if (hex.length() < 6) {
            hex = "0" + hex;
        }
        hex = "#" + hex;
        System.out.println(hex);
        return hex;
    }

    public String RGBAtoHexConverter(String fontValue) {

        String s = fontValue.substring(fontValue.indexOf("(") + 1);
        s = fontValue.substring(0, fontValue.indexOf(")") + 1);

        String r = s.substring(5, 8);
        String g = s.substring(10, 13);
        String b = s.substring(15, 18);

        int rvalue = Integer.parseInt(r);
        int bvalue = Integer.parseInt(b);
        int gvalue = Integer.parseInt(g);

        LuminanceValue2 = (0.2126 * rvalue) + (0.7152 * gvalue) + (0.0722 * bvalue);
        System.out.println(LuminanceValue2);
        
        Color color = new Color(rvalue, gvalue, bvalue);
        String hex = Integer.toHexString(color.getRGB() & 0xFFFFFF);
        if (hex.length() < 6) {
            hex = "0" + hex;
        }
        hex = "#" + hex;
        System.out.println(hex);
        return hex;
    }
    
    public double ContrastCalculator(double l1, double l2){
        
        System.out.println("l1->" + l1);
        System.out.println("l2->" + l2);
        
        if(l1>l2){            
            return (l2/l1) * 10;
        }else{            
            return (l1/l2) * 10;
        }        
    }

    @Test
    public void test_backgroundColorWithMaintTitle() throws Exception {

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        Thread.sleep(5000);
        //Fetch color from the background for the iframe sign-up-form
        backGroundColor = driver.findElement(By.xpath("//*[@id=\"main-site\"]/section[2]/div/div[2]/div")).getCssValue("background");

        //Fetch color from the background for the iframe sign-up-form
        driver.switchTo().frame("wufooFormm7x4z5");
        fontColor = driver.findElement(By.xpath("//*[@id=\"Field26\"]")).getCssValue("color");     
                
        RGBtoHexConverter(backGroundColor);
        RGBAtoHexConverter(fontColor);
        
        
        Assert.assertTrue("Verify that the color contrast ratio is 4.5:1, the Value is: " + ContrastCalculator(LuminanceValue1,LuminanceValue2), ContrastCalculator(LuminanceValue1,LuminanceValue2) == 4.5 );
        
        //Close the browser
        driver.quit();
    }

}
