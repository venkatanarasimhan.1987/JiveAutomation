/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import org.junit.Test;
import junit.framework.Assert;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 *
 * @author venkata_narasimhan
 */
public class JiveEmailValidator {

    private static WebDriver driver = null;
    public static String URL = "https://jive.com/resources/support/submit-a-ticket/";

    @Test
    public void test_CheckforSpecialATvalue() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);

        Assert.assertTrue("Verify that the @ value exists in the email - FINAL", obj.EMAILVALUE.contains("@"));
        String domain = obj.EMAILVALUE.substring(obj.EMAILVALUE.indexOf("@"));
        domain = domain.substring(domain.indexOf("."));
        int length = domain.length();

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        driver.close();
    }

    @Test
    public void test_CheckforSpecialDomainValue() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);

        int countDots = StringUtils.countMatches(obj.EMAILVALUE, ".");

        String domain = obj.EMAILVALUE.substring(obj.EMAILVALUE.indexOf("@"));
        do {
            domain = domain.substring(domain.indexOf("."));
            countDots = StringUtils.countMatches(domain, ".");
        } while (countDots > 1);
        
        System.out.println("The domain value is: " + domain);
        int length = domain.length();
        System.out.println("The domain length value is: " + length);
        Assert.assertTrue("Verify that the email has atleast 2 chars for domain levels elements - FINAL", length >= 3);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        driver.close();
    }
}
