
import junit.framework.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author venkata_narasimhan
 */
public class JiveFunctionalTests255Chars {
    
    private static WebDriver driver = null;
    public static String URL = "https://jive.com/resources/support/submit-a-ticket/";

    @Test
    public void test_formFillingFillName255Chars() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE_255);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();

        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        
        driver.close();
    }
    
    @Test
    public void test_formFillingFillEmail255Chars() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE_255);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();

        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());

        driver.close();
    }
    
    @Test
    public void test_formFillingFillCompanyName255Chars() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE_255);

        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();

        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());

        driver.close();
    }
    
    @Test
    public void test_formFillingFillPhoneWithChars() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-running-insecure-content");

        System.setProperty("webdriver.chrome.driver", "C:\\tools\\SeleniumGrid\\chromedriver.exe");
        driver = new ChromeDriver(options);
        //Full screen 
        driver.manage().window().maximize();
        //Opening submit a ticket home page.
        driver.get(URL);
        //Switch to Frame:
        driver.switchTo().frame("wufooFormm7x4z5");
        JiveAllStrings obj = new JiveAllStrings();
        
        //Check for the values in the textbox for phone 1
        driver.findElement(By.id("Field27")).sendKeys("1");
        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown on the modal head", driver.findElement(By.id("fo2li26")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1a", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));
        driver.findElement(By.id("Field27")).clear();
        
        
        driver.findElement(By.id("Field27")).sendKeys("11");
        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown on the modal head", driver.findElement(By.id("fo2li26")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 1b", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));
        driver.findElement(By.id("Field27")).clear();
        
        
        driver.findElement(By.id("Field27")).sendKeys("1222");
        driver.findElement(By.id("Field27-1")).sendKeys("1222");
        driver.findElement(By.id("Field27-2")).sendKeys("1222");
        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error message is shown in modal 1c", !driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));
               
        driver.findElement(By.id("Field27")).clear();
        driver.findElement(By.id("Field27-1")).clear();
        driver.findElement(By.id("Field27-2")).clear();
        
        
        //Check for the values in the textbox for phone 2
        driver.findElement(By.id("Field27-1")).sendKeys("1");
        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown on the modal head", driver.findElement(By.id("fo2li26")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 2a", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));
        driver.findElement(By.id("Field27-1")).clear();
        
        
        driver.findElement(By.id("Field27-1")).sendKeys("11");
        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown on the modal head", driver.findElement(By.id("fo2li26")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal 2b", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));
        driver.findElement(By.id("Field27-1")).clear();
        
        driver.findElement(By.id("Field27")).sendKeys("1222");
        driver.findElement(By.id("Field27-1")).sendKeys("1222");
        driver.findElement(By.id("Field27-2")).sendKeys("1222");
        //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error message is shown in modal 2c", !driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));        
        
        driver.findElement(By.id("Field27")).clear();
        driver.findElement(By.id("Field27-1")).clear();
        driver.findElement(By.id("Field27-2")).clear();
        
        //Check for the values in the textbox for phone 3
        driver.findElement(By.id("Field27-2")).sendKeys("1");
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown on the modal head", driver.findElement(By.id("fo2li26")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal -3a", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));
        driver.findElement(By.id("Field27-2")).clear();
        
        driver.findElement(By.id("Field27-2")).sendKeys("11");
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown on the modal head", driver.findElement(By.id("fo2li26")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal -3b", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));
        driver.findElement(By.id("Field27-2")).clear();
        
        driver.findElement(By.id("Field27-2")).sendKeys("111");
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the error modal is shown", driver.findElement(By.id("errorLi")).isDisplayed());
        Assert.assertTrue("Verify that the error message label is shown", driver.findElement(By.id("errorMsgLbl")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown", driver.findElement(By.id("errorMsg")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown on the modal head", driver.findElement(By.id("fo2li26")).isDisplayed());
        Assert.assertTrue("Verify that the error message is shown in modal -3c", driver.getPageSource().contains(obj.ERRORMESSAGEINMODALFORPHONE));
        driver.findElement(By.id("Field27-2")).clear();
        
        driver.findElement(By.id("Field27")).sendKeys("1222");
        driver.findElement(By.id("Field27-1")).sendKeys("1222");
        driver.findElement(By.id("Field27-2")).sendKeys("1222");       

        driver.findElement(By.id("Field26")).sendKeys(obj.NAMEVALUE);
        driver.findElement(By.id("Field12")).sendKeys(obj.EMAILVALUE);
        driver.findElement(By.id("Field14")).sendKeys(obj.COMPANYNAMEVALUE);
        
         //Click the next button and redirect to new iFrame
        driver.findElement(By.id("nextPageButton")).click();
        Assert.assertTrue("Verify that the 2nd Page is open - FINAL", driver.findElement(By.id("saveForm")).isDisplayed());

        driver.close();
    }   
    
}
